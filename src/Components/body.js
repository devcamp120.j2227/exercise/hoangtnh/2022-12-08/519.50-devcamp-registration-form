import Row1 from "./BodyRow/BodyRow1";
import Row2 from "./BodyRow/BodyRow2";
import Row3 from "./BodyRow/BodyRow3";
import Row4 from "./BodyRow/BodyRow4";

function Body () {
    return(
        <>
            <Row1/>
            <Row2/>
            <Row3/>
            <Row4/>
        </>
    )
}

export default Body;