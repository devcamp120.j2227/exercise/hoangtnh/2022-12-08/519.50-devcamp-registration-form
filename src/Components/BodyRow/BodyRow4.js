import {Button, Col, Row} from "reactstrap"
function Row4 () {
    return(
        <>
            <Row className="justify-content-end">
                <Col sm={2}>
                    <Button style={{width:"100%"}} color="success">
                        Check data
                    </Button>
                </Col>
                <Col sm={2}>
                    <Button style={{width:"100%"}} color="success">
                        Send
                    </Button>
                </Col>
            </Row>
        </>
    )
}

export default Row4;