import {Row, Col, Form, FormGroup, Label, Input} from "reactstrap"
function Row3 () {
    return(
        <>
            <Row sm={12}>
                <Form>
                    <FormGroup row>
                        <Col sm={2} className="d-flex align-items-center">
                            <Label for="firstName">
                                Subject:
                            </Label>
                        </Col>
                            <Col sm={10}>
                                <Input id="firstName" type="textarea"/>
                            </Col>
                    </FormGroup>
                </Form>
            </Row>
        </>
    )
}

export default Row3;