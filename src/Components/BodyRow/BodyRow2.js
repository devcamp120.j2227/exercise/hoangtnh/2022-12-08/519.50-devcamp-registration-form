import {Col, Form, FormGroup, Input, Label, Row} from "reactstrap"
function Row2 () {
    return(
        <>
            <Row sm={12}>
                <Col sm={6}>
                    <Form>
                        <FormGroup row>
                            <Col sm={4} className="d-flex align-items-center">
                                <Label for="firstName">
                                    First Name <span style={{color:"red"}}>(*)</span>
                                </Label>
                            </Col>
                                <Col sm={8}>
                                    <Input id="firstName" type="name"/>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Col sm={4} className="d-flex align-items-center">
                                    <Label for="lastname">
                                        Last Name <span style={{color:"red"}}>(*)</span>
                                    </Label>
                                </Col>
                                <Col sm={8}>
                                    <Input id="lastname" type="name"/>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Col sm={4} className="d-flex align-items-center">
                                    <Label for="birthday">
                                        Birthday <span style={{color:"red"}}>(*)</span>
                                    </Label>
                                </Col>
                                <Col sm={8}>
                                    <Input id="birthday" type="text"/>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Col sm={4} className="d-flex align-items-center">
                                    <Label for="gender">
                                        Gender <span style={{color:"red"}}>(*)</span>
                                    </Label>
                                </Col>
                            <Col sm={8}>
                                <Row>
                                    <Col sm={6}>
                                        <FormGroup check>
                                            <Input
                                                name="radio1"
                                                type="radio"
                                            />
                                            {' '}
                                            <Label check>
                                                Male
                                            </Label>
                                        </FormGroup>
                                        </Col>
                                    <Col sm={6}>
                                        <FormGroup check>
                                            <Input
                                                name="radio1"
                                                type="radio"
                                            />
                                            {' '}
                                            <Label check>
                                                Female
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </Col>
                        </FormGroup>
                    </Form>
                </Col>
                <Col sm={6}>
                    <Form>
                        <FormGroup row>
                            <Col sm={4} className="d-flex align-items-center">
                                <Label for="passport">
                                    Passport <span style={{color:"red"}}>(*)</span>
                                </Label>
                            </Col>
                            <Col sm={8}>
                                <Input id="passport" type="text"/>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col sm={4} className="d-flex align-items-center">
                                <Label for="email">
                                    Email <span style={{color:"red"}}>(*)</span>
                                </Label>
                            </Col>
                            <Col sm={8}>
                                <Input id="email" type="email"/>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col sm={4} className="d-flex align-items-center">
                                <Label for="country">
                                    Country <span style={{color:"red"}}>(*)</span>
                                </Label>
                            </Col>
                            <Col sm={8}>
                                <Input id="country" type="text"/>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Col sm={4} className="d-flex align-items-center">
                                <Label for="region">
                                    Region 
                                </Label>
                            </Col>
                            <Col sm={8}>
                                <Input id="region" type="text"/>
                            </Col>
                        </FormGroup>
                    </Form>
                </Col>
            </Row>
        </>
    )
}

export default Row2;