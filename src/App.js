import "bootstrap/dist/css/bootstrap.min.css"
import { Container } from "reactstrap";
import Body from "./Components/body";

function App() {
  return (
    <Container>
      <Body/>
    </Container>
  );
}

export default App;
